# Tablette pour laptop

Une tablette pour surélever un ordinateur portable, réalisée pour et avec nos voisins et amis de [Débutant-e Accepté-e](https://www.debutant-e.pro/)

Le modèle a été réalisé en 3D avec FreeCAD de manière paramétrique permettant de redimensionner et d'adapter la tablette à souhait.

![alt text](img/tablette1.png)

Les esquisses ont ensuite été exportées au format SVG afin d'inclure les éléments de gravure et les trous pour aération avec Inkscape puis enfin exportées en DXF pour réaliser une découpe laser dans du contreplaqué peuplier de 5mm d'épaisseur.

![alt text](img/photo1.jpg)

![alt text](img/photo2.jpg)

![alt text](img/photo3.jpg)

![alt text](img/photo4.jpg)

